//////////////////////////////////////////////////////////////
// Ui that demonstrates buttons becoming non-blockers,
// even though they have onMouseDown properties.
//
// Annotations below in comments marked !CF
//////////////////////////////////////////////////////////////

import { Color4 } from '@dcl/sdk/math'
import ReactEcs, { Button, Label, ReactEcsRenderer, UiEntity } from '@dcl/sdk/react-ecs'

export function setupUi() {
    ReactEcsRenderer.setUiRenderer(uiComponent)
}

const FULL_WIDTH = 200
const FULL_HEIGHT = 225
const FULL_POS_X = 0
const FULL_POS_Y = 120

const ICON_WIDTH = 100
const ICON_HEIGHT = 72
const ICON_POS_X = 0
const ICON_POS_Y = 120

const BUTTON_WIDTH = 70
const BUTTON_HEIGHT = 30

const COL1_X = 95
const COL3_X = 13

const ROW1_Y = 182 // up 41
const ROW5_Y = 27
const ROW6_Y = 4

const FULL_CAPTION_X = 10
const FULL_CAPTION_WIDTH = 60
const FULL_CAPTION_HEIGHT = 20

const ICON_CAPTION_X = 0
const ICON_CAPTION_WIDTH = 33
const ICON_CAPTION_HEIGHT = 15

type UiMode = 'expanded'|'icon'

let uiMode:UiMode = 'icon'

function expandedUi() {
    return <UiEntity
        uiTransform={{
            positionType: 'absolute',
            width: FULL_WIDTH,
            height: FULL_HEIGHT,
            position: { right: FULL_POS_X, bottom: FULL_POS_Y },
        }}
        uiBackground={{ color: Color4.fromHexString("#70ac76ff") }}
    >
        <Button 
            value='Button 2'
            onMouseDown={()=>{ 
                console.log("Button 2 Pressed")
            }}
            // color={Color4.Black()}
            uiTransform={{
                positionType: 'absolute',
                width: BUTTON_WIDTH,
                height: BUTTON_HEIGHT,
                position: { right: COL1_X, bottom: ROW1_Y }
                // ,
                // pointerFilter:'block' // !CF this is required despite there being an onMouseDown property in this button
            }}
        ></Button>        
        <Button 
            value='ToggleUi'
            onMouseDown={()=>{ 
                uiMode = 'icon'; 
            }}
            // color={Color4.Black()}
            uiTransform={{
                positionType: 'absolute',
                width: BUTTON_WIDTH,
                height: BUTTON_HEIGHT,
                position: { right: COL3_X, bottom: ROW5_Y }
            }}
            // uiBackground={{
            //     color:Color4.White(),
            // }}
        ></Button>
        <UiEntity 
            uiTransform={{ 
                positionType: 'absolute',
                width: FULL_CAPTION_WIDTH, 
                height: FULL_CAPTION_HEIGHT,
                position: { right: FULL_CAPTION_X, bottom: ROW6_Y } 
            }} >
            <Label
                value="Expanded UI"
                color={Color4.White()}
                fontSize={14}
                font="sans-serif"
                textAlign="middle-center"
            />
        </UiEntity>
    </UiEntity>
}

function iconUi() {
    return <UiEntity
        uiTransform={{
            positionType: 'absolute',        
            width: ICON_WIDTH,
            height: ICON_HEIGHT,
            position: { right: ICON_POS_X, bottom: ICON_POS_Y }, 
        }}
        uiBackground={{ color: Color4.fromHexString("#70ac76ff") }}
    >
        <Button 
            value='Toggle UI' // using text only as documentation (button label)here.
            onMouseDown={()=>{ 
                uiMode = 'expanded'; 

            }}
            color={Color4.Black()}//fromHexString("#FFFFFF00")} // to hide the text
            uiTransform={{
                positionType: 'absolute',
                width: BUTTON_WIDTH,
                height: BUTTON_HEIGHT,
                position: { right: COL3_X, bottom: ROW5_Y }
                // ,
                // pointerFilter:'block' // !CF this is required despite there being an onMouseDown property in this button
            }}
        ></Button>
        <UiEntity 
            uiTransform={{ 
                positionType: 'absolute',
                width: ICON_CAPTION_WIDTH, 
                height: ICON_CAPTION_HEIGHT,
                position: { right: ICON_CAPTION_X, bottom: ROW6_Y } 
            }} >
            <Label
                value="Small UI"
                color={Color4.White()}
                fontSize={10}
                font="sans-serif"
                textAlign="middle-center"
            />
        </UiEntity>        
    </UiEntity>
}

function uiComponent (){
    switch (uiMode) {
        case 'icon':
            return iconUi()
        case 'expanded':
            return expandedUi()
    }
}



