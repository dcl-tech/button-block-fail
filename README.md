# button-block-fail
Demonstrates an issue introduced in DCL SDK7 release 7.3.24
in which buttons fail to be blocking, 
even if they have onMouseDown properties.

The issue is reproducable in 7.3.24 and 7.3.25, but not in 7.3.23

To demonstrate the issue:
1) Run the scene
2) Notice that the Toggle UI button is blocking.  
3) Click it // the UI expands
4) !!! Button 2 is not blocking (even though it has an onMouseDown property)
5) Click the Toggle UI button // the UI becomes small again
6) !!! Now the original Toggle UI button on the the Small UI is no longer blocking




